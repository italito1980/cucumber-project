@End2End
Feature: The Login Page
	As a client
	I want to login the page
	So that I can see the welcome page
	
	Background:
	Given The user goes to the login page of the web site
	
	@ignore
	Scenario: The user logs in with valid credentials
	When  The user fill the form with valid credentials
	Then  The user must see the welcome page
	
	@ignore
	Scenario: The user logs in with invalid credentials
	When The user fill the form with invalid credentials
	Then The user must see an error message on the page
	And The user is not able to get into the system

	@ignore
	Scenario: The user is able to log in
	When The user enters username "Guatemala"
	And The user enters password "Mexico"
	And The user clicks on login
	Then  The user must see the welcome page
	
	@ignore
	Scenario Outline: The user is able to login multiple
	When The user enters "<username>" and "<password>"
	And The user clicks on login
	Then  The user must see the welcome page
	
	Examples:
	|username|password|
	|Guatemala|Mexico|
	|Colombia|Peru|
	
	
	Scenario: The user is able to login multiple
	When The user fills username and password
	|username|password|
	|Guatemala|Mexico|
	|Colombia|Peru|	
	And The user clicks on login
	Then  The user must see the welcome page