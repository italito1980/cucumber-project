package com.mytests.cucumber.selenium.test.util;

public class Driver {
	
	public static String [] CHROME= {"webdriver.chrome.driver","C:\\selenium\\seleniumBrowserDrivers\\chromedriver.exe"};
	public static String [] FIREFOX= {"webdriver.gecko.driver","C:\\selenium\\seleniumBrowserDrivers\\geckodriver.exe"};
	public static String [] EDGE= {"webdriver.edge.driver","C:\\selenium\\seleniumBrowserDrivers\\MicrosoftWebDriver.exe"};
}