package com.mytests.cucumber.selenium.test.util;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

public class SeleniumBrowserUtils {
	
	private WebDriver driver;

	public SeleniumBrowserUtils() {
		
	}
	
	public WebDriver createNewDriver(String browser) {
		Browsers browserString = Browsers.valueOf(browser);
		switch (browserString) {
		case chrome:
			System.setProperty(Driver.CHROME[0], Driver.CHROME[1]);
			driver = new ChromeDriver();
			break;
		case firefox:
			System.setProperty(Driver.FIREFOX[0], Driver.FIREFOX[1]);
			driver = new FirefoxDriver();
			break;
		case edge:
			System.setProperty(Driver.EDGE[0], Driver.EDGE[1]);
			driver = new EdgeDriver();
			break;
		case junit:
			driver = new HtmlUnitDriver();
		}
		
			
		return driver;
	}
	
	public void openWebSite(String site) {
		driver.get(site);
	}
	
	public void closeBrowser() {
		driver.close();
	}
	
	public void quitBrowser() {
		driver.quit();
	}
}
