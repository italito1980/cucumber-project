package com.mytests.cucumber.selenium.test.pom;

import java.util.List;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ViajesCredomaticSearchFlight {
	
	WebDriver driver;
	
	@FindBy(xpath = "//input[@name='cityFrom']")
	private WebElement from;
	
	@FindBy(how = How.XPATH, using = "//input[@name='cityTo']")
	private WebElement to;
	
	@FindBy(xpath = "//input[@name='dateStart']")
	private WebElement dateStart;
	
	@FindBy(how = How.XPATH, using="//select[@class='ui-datepicker-month']")
	private WebElement monthDropdown;

	@FindBy(how = How.XPATH, using="//div[@class='ui-datepicker-title']/select[2]")
	private WebElement yearDropdown;

	@FindAll({
		@FindBy(xpath = "//a[@class='ui-state-default']")
	})
	private List<WebElement> calendarDaysList;
	
	@FindBy(xpath = "//input[@name='dateEnd']")
	private WebElement dateEnd;

	@FindBy(how = How.XPATH, using="(//div[@class='col-md-6']/select)[1]")
	private WebElement passangerDropdown;

	@FindBy(how = How.XPATH, using="(//div[@class='col-md-6']/select)[2]")
	private WebElement under18PassangerDropdown;

	@FindBy(xpath = "//div[@class='col-md-12']/button")
	private WebElement searchButton;
	
	@FindBy(xpath = "(//span[@class='ptsFont'])[1]")
	private WebElement textToValidate;
	
	public ViajesCredomaticSearchFlight(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	
	public void setFrom(String from) {
		this.from.sendKeys(from);
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		this.from.sendKeys(Keys.ARROW_DOWN);
		this.from.sendKeys(Keys.ENTER);
	}
	
	public String getFromValue() {
		return this.from.getText();
	}

	public String getFromHiddenValue() {
		JavascriptExecutor executor = (JavascriptExecutor)this.driver;
		String javaScriptCommand = "return document.getElementsByName(\"cityFrom\")[0].value;";
		return (String)executor.executeScript(javaScriptCommand);
	}


	public void setTo(String to) {
		this.to.sendKeys(to);
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		this.to.sendKeys(Keys.ARROW_DOWN);
		this.to.sendKeys(Keys.ENTER);
	}
	
	public String getToValue() {
		return this.to.getText();
	}

	public String getToHiddenValue() {
		JavascriptExecutor executor = (JavascriptExecutor)this.driver;
		String javaScriptCommand = "return document.getElementsByName(\"cityTo\")[0].value;";
		return (String)executor.executeScript(javaScriptCommand);
	}

	private void clickOnDateStartCalendar() {
		this.dateStart.click();
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void selectMonthDateStart(String month) {
		Select monthDropdown = new Select(this.monthDropdown);
		monthDropdown.selectByVisibleText(month);
	}

	public void selectYearDateStart(String year) {
		Select yearDropdown = new Select(this.yearDropdown);
		yearDropdown.selectByVisibleText(year);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void selectDayDateStart(String day) {
		for (int i = 0; i < this.calendarDaysList.size(); i++) {
			String dayValue = calendarDaysList.get(i).getText();
			if(dayValue.equals(day)) {
				calendarDaysList.get(i).click();
			}
		}
	}

	private void clickOnDateEndCalendar() {
		this.dateEnd.click();
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void selectMonthDateEnd(String month) {
		Select monthDropdown = new Select(this.monthDropdown);
		monthDropdown.selectByVisibleText(month);
	}

	public void selectYearDateEnd(String year) {
		Select yearDropdown = new Select(this.yearDropdown);
		yearDropdown.selectByVisibleText(year);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void selectDayDateEnd(String day) {
		for (int i = 0; i < this.calendarDaysList.size(); i++) {
			String dayValue = calendarDaysList.get(i).getText();
			if(dayValue.equals(day)) {
				calendarDaysList.get(i).click();
				break;
			}
		}
	}

	public void selectStartDate(String day, String month, String year) {
		clickOnDateStartCalendar();
		selectYearDateStart(year);
		selectMonthDateStart(month);
		selectDayDateStart(day);
	}

	public void selectEndDate(String day, String month, String year) {
		clickOnDateEndCalendar();
		selectYearDateEnd(year);
		selectMonthDateEnd(month);
		selectDayDateEnd(day);
	}


	public void selectPassangers(String passangers) {
		Select passangerDropdown = new Select(this.passangerDropdown);
		passangerDropdown.selectByVisibleText(passangers);
	}

	public void selectUnder18Passangers(String under18Passangers) {
		Select under18PassangerDropdown = new Select(this.under18PassangerDropdown);
		under18PassangerDropdown.selectByVisibleText(under18Passangers);
	}
	
	public Boolean isTextPresent(String text) {
		WebDriverWait wait = new WebDriverWait(this.driver, 30);
		return wait.until(ExpectedConditions.textToBePresentInElement(this.textToValidate, text));		
	}

	public String getPageTitle() {
		return this.driver.getTitle();
	}
	
	public void searchFlights() {
		this.searchButton.click();
	}
	
	public void openSite() {
		this.driver.get("https://www.viajesbaccredomatic.com/");
	}

}
