package com.mytests.cucumber.stepdefinitions;

import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import com.mytests.cucumber.selenium.test.pom.ViajesCredomaticSearchFlight;
import com.mytests.cucumber.selenium.test.util.Browsers;
import com.mytests.cucumber.selenium.test.util.SeleniumBrowserUtils;
import com.mytests.cucumber.stepdefinitions.hooks.Hooks;

import cucumber.api.Result.Type;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.cucumber.datatable.DataTable;

public class LoginSteps2 {

	private ViajesCredomaticSearchFlight viajesCredomaticPage;
	
	public LoginSteps2(Hooks hooks) {
		viajesCredomaticPage = hooks.setUp();
	}
	
//	@Given("The user goes to the login page of the web site")
//	public void user_login_page() {
//		viajesCredomaticPage.openSite();		
//	}
//	
//	@When("The user fill the form with valid credentials")
//	public void user_fill_form_with_valid_credentials() {
//		viajesCredomaticPage.setFrom("Guatemala");
//		viajesCredomaticPage.setTo("Mexico");
//	}
//	
//	@Then("The user must see the welcome page")
//	public void user_sees_welcome_page() {
//		Assert.assertTrue(viajesCredomaticPage.getFromHiddenValue().contains("Guatemala"));
//	}
//	
//	@When("The user fill the form with invalid credentials")
//	public void user_fill_form_with_invalid_credentials() {
//		viajesCredomaticPage.setFrom("Guatemala");
//		viajesCredomaticPage.setTo("Mexico");
//	}
//	
//	@Then("The user must see an error message on the page")
//	public void user_sees_error_message_on_page() {
//		Assert.assertTrue(!viajesCredomaticPage.getToHiddenValue().contains("Colombia"));
//	}
//	
//	@And("The user is not able to get into the system")
//	public void user_not_able_to_get_into_the_system() {
//		Assert.assertTrue(!viajesCredomaticPage.getToHiddenValue().contains("Peru"));
//	}
//	
//	@When("^The user enters username \"(.*)\"$")
//	public void user_enters_usermane(String username) {
//		viajesCredomaticPage.setFrom(username);		
//	}
//	@And("The user enters password \"(.*)\"$")
//	public void user_enters_password(String password) {
//		viajesCredomaticPage.setTo(password);
//	}
//	
//	@And("The user clicks on login")
//	public void user_clicks_on_login() {
//		Assert.assertTrue(viajesCredomaticPage.getFromHiddenValue().contains("Guatemala"));		
//	}
//	
	@When("^The user enters \"(.*)\" and \"(.*)\"$")
	public void user_enters_username_and_password(String username, String password) {
		viajesCredomaticPage.setFrom(username);
		viajesCredomaticPage.setTo(password);		
	}

//	@When("^The user fills username and password")
//	public void user_enters_username_and_password_table(DataTable credentials) {
//		for (Map<Object, Object> data : credentials.asMaps(String.class, String.class)) {
//			String username = (String)data.get("username");
//			String password = (String)data.get("password");
//			viajesCredomaticPage.setFrom(username);
//			viajesCredomaticPage.setTo(password);	
//		}
//	}
//	
}
