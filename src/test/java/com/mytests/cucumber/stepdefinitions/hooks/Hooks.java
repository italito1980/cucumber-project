package com.mytests.cucumber.stepdefinitions.hooks;

import java.io.File;

import org.apache.maven.reporting.MavenReportException;
import org.openqa.selenium.WebDriver;

import com.cucumber.listener.Reporter;
import com.github.mkolisnyk.cucumber.reporting.CucumberResultsOverview;
import com.github.mkolisnyk.cucumber.reporting.CucumberUsageReporting;
import com.mytests.cucumber.selenium.test.pom.ViajesCredomaticSearchFlight;
import com.mytests.cucumber.selenium.test.util.SeleniumBrowserUtils;

import cucumber.api.java.After;
import cucumber.api.java.Before;

public class Hooks {
	
	private WebDriver driver;
	private SeleniumBrowserUtils utils;
	private ViajesCredomaticSearchFlight viajesCredomaticPage;
	
	@Before
	public ViajesCredomaticSearchFlight setUp() {				
		if(viajesCredomaticPage == null) {
			utils = new SeleniumBrowserUtils();
			driver =  utils.createNewDriver("chrome");
			viajesCredomaticPage = new ViajesCredomaticSearchFlight(driver);		
		}
		return viajesCredomaticPage;
	}
	
	@After
	public void tearDown() {
		driver.quit();
		driver = null;
	}
}
