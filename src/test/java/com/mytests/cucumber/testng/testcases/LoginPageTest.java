package com.mytests.cucumber.testng.testcases;

import org.apache.maven.reporting.MavenReportException;
import org.testng.annotations.AfterTest;

import com.github.mkolisnyk.cucumber.reporting.CucumberUsageReporting;
import com.github.mkolisnyk.cucumber.runner.ExtendedCucumberOptions;

import cucumber.api.CucumberOptions;


import cucumber.api.testng.AbstractTestNGCucumberTests;

@CucumberOptions(features= {"src/test/java/com/mytests/cucumber/features/"}, 
				 glue= {"com/mytests/cucumber/stepdefinitions"},
				 tags= {"@End2End","not @ignore"},
				 plugin= {"json:target/cucumber-usage.json"
//						  "html:target/cucumber-html-report",
//						  "json:target/cucumber-report.json"
//						  "com.cucumber.listener.ExtentCucumberFormatter:target/ExtentReport.html"
						 },
				 monochrome=true,
				 dryRun=false)

//@ExtendedCucumberOptions(jsonReport = "target/cucumber.json",
//						 jsonUsageReport = "target/cucumber-usage.json",
//						 usageReport = true,
//						 outputFolder = "target")
public class LoginPageTest extends AbstractTestNGCucumberTests{

}


