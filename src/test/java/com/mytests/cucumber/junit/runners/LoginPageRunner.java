package com.mytests.cucumber.junit.runners;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
	
@RunWith(Cucumber.class)
@CucumberOptions(features= {"src/test/java/com/mytests/cucumber/features/"}, 
					glue= {"com/mytests/cucumber/stepdefinitions"},
					tags= {"@End2End","not @ignore"},
					plugin= {
//							  "html:target/cucumber-html-report",
//							  "json:target/cucumber-report.json",
							  "com.cucumber.listener.ExtentCucumberFormatter:target/ExtentReport.html"},
					monochrome=true,
					dryRun=false)

public class LoginPageRunner {

}
